﻿USE jardineria; --  ejemplo5u3

/* 
  Ej1 - Escribe un procedimiento que reciba el nombre de un país
        como parámetro de entrada y realice una consulta sobre la
        tabla cliente para obtener todos los clientes que existen
        en la tabla de ese país. 
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej1(v_pais varchar(30))
    COMMENT 'Escribe un procedimiento que reciba el nombre de un país
             como parámetro de entrada y realice una consulta sobre la
             tabla cliente para obtener todos los clientes que existen
             en la tabla de ese país.'
    BEGIN
      SELECT c.nombre_cliente FROM cliente c WHERE pais=v_pais;
    END //
DELIMITER ;

CALL ej1('Spain');


/*
  Ej2 - Realizar una función que me devuelva el número de registros
        de la tabla clientes que pertenezcan a un país que le pase por teclado.   
*/
DELIMITER //
  CREATE OR REPLACE FUNCTION ej2(v_pais varchar(20))
    RETURNS int
    COMMENT 'Realizar una función que me devuelva el número de registros
             de la tabla clientes que pertenezcan a un país que le pase por teclado.'
  BEGIN    
    RETURN (SELECT COUNT(*) FROM cliente c WHERE pais=v_pais);
  END //
DELIMITER ;

SELECT ej2('spain');
 
/*
  Ej3 - Realizar el mismo ejercicio anterior, pero para desplazarme
        por la tabla utilizar cursores con excepciones.   
*/
DELIMITER //
  CREATE OR REPLACE FUNCTION ej3(v_pais varchar(20))
    RETURNS int
    COMMENT 'Realizar el mismo ejercicio anterior, pero para desplazarme
             por la tabla utilizar cursores con excepciones.'
  BEGIN
      DECLARE cont int DEFAULT 0;
      DECLARE final int DEFAULT 0;
      DECLARE result int DEFAULT 0;
      
      DECLARE cursor1 CURSOR FOR SELECT c.nombre_cliente FROM cliente c WHERE c.pais=v_pais;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET final=1;
      
      OPEN cursor1;

      FETCH cursor1 INTO result;

      WHILE final<>1 DO 
        SET cont = cont+1;
        FETCH cursor1 INTO result;
      END WHILE;
      
    RETURN cont;
  END //
DELIMITER ;

SELECT ej3('spain');

/*
  Ej4 - Escribe una función que le pasas una cantidad y que te
        devuelva el número total de productos que hay en la tabla
        productos cuya cantidad en stock es superior a la pasada.   
*/
DELIMITER //
  CREATE OR REPLACE FUNCTION ej4(cantidad int)
    RETURNS int
    COMMENT 'Escribe una función que le pasas una cantidad y que te
             devuelva el número total de productos que hay en la tabla
             productos cuya cantidad en stock es superior a la pasada.'
  BEGIN        
    RETURN (SELECT 
              SUM(p.cantidad_en_stock) 
              FROM
                producto p 
              WHERE 
                p.cantidad_en_stock>cantidad);
  END //
DELIMITER ;
SELECT ej4(20);

/*
  Ej5 - Realizar el mismo ejercicio anterior pero con cursores 
*/
DELIMITER //
  CREATE OR REPLACE FUNCTION ej5(cantidad int)
    RETURNS int
    COMMENT 'Realizar el mismo ejercicio anterior pero con cursores'
  BEGIN
    DECLARE result int DEFAULT 0;
    DECLARE final int DEFAULT 0;
    DECLARE acumulador int DEFAULT 0;

    DECLARE cursor1 CURSOR FOR SELECT p.cantidad_en_stock FROM producto p WHERE p.cantidad_en_stock>cantidad;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET final=1;

    OPEN cursor1;

    FETCH cursor1 INTO acumulador;

    WHILE final<>1 DO     
      SET result= result+acumulador;  
      FETCH cursor1 INTO acumulador;                              
    END WHILE;

    CLOSE cursor1;

    RETURN result;
  END //
DELIMITER ;

SELECT ej5(20); 


/*
  ej6 - Escribe un procedimiento que reciba como parámetro de entrada una forma de pago,
        que será una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc).
        Y devuelva como salida el pago de máximo valor realizado para esa forma de pago. 
        Deberá hacer uso de la tabla pago de la base de datos jardinería.   
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej6(pago varchar(30))
    COMMENT 'Escribe un procedimiento que reciba como parámetro de entrada una forma de pago,
             que será una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc).
             Y devuelva como salida el pago de máximo valor realizado para esa forma de pago. 
             Deberá hacer uso de la tabla pago de la base de datos jardinería.'
    BEGIN
      SELECT MAX(total) FROM pago p WHERE forma_pago=pago;
    END //
DELIMITER ;

CALL ej6('transferencia');

/* 
  ej7 - Escribe un procedimiento que reciba como parámetro de entrada una forma de pago,
        que será una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc).
        Y devuelva como salida los siguientes valores teniendo en cuenta la forma de pago seleccionada como parámetro de entrada:  
        
        - el pago de máximo valor,
        - el pago de mínimo valor, 
        - el valor medio de los pagos realizados,
        - la suma de todos los pagos, 
        - el número de pagos realizados para esa forma de pago. 

        Deberá hacer uso de la tabla pago de la base de datos jardineria. 
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej7(
                                  pago varchar(20),
                                  OUT maximo decimal(15,2), 
                                  OUT minimo decimal(15,2), 
                                  OUT media decimal(15,2),
                                  OUT suma decimal(15,2),
                                  OUT contador decimal(15,2)
                                 )
    COMMENT 'Escribe un procedimiento que reciba como parámetro de entrada una forma de pago,
             que será una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc).
             Y devuelva como salida los siguientes valores teniendo en cuenta la forma de pago seleccionada como parámetro de entrada:  
              
             - el pago de máximo valor,
             - el pago de mínimo valor, 
             - el valor medio de los pagos realizados,
             - la suma de todos los pagos, 
             - el número de pagos realizados para esa forma de pago. 
      
             Deberá hacer uso de la tabla pago de la base de datos jardineria.'
    BEGIN

    DECLARE cursor1 CURSOR FOR SELECT 
        MAX(total) maximo,
        MIN(total) minimo,
        AVG(total) medio,
        SUM(total) suma,
        COUNT(total) contador        
        FROM 
          pago p 
        WHERE 
          forma_pago=pago;

    DECLARE EXIT HANDLER FOR NOT FOUND CLOSE cursor1;

    OPEN cursor1 ;

    FETCH cursor1 INTO maximo,minimo,media,suma,contador;

    CLOSE cursor1;

    END //
DELIMITER ;

CALL ej7('Paypal', @max, @min, @avg, @sum, @cont);
SELECT @max, @min, @avg, @sum, @cont;

/*
  Ej8 - Escribe una función para la tabla productos que devuelva el valor medio
        del precio de venta de los productos de un determinado proveedor 
        que se recibirá como parámetro de entrada. 
        El parámetro de entrada será el nombre del proveedor. 
*/
DELIMITER //
  CREATE OR REPLACE FUNCTION ej8(proveedor varchar(60))
    RETURNS float
    COMMENT 'Escribe una función para la tabla productos que devuelva el valor medio
             del precio de venta de los productos de un determinado proveedor 
             que se recibirá como parámetro de entrada. 
             El parámetro de entrada será el nombre del proveedor.'
  BEGIN
    RETURN (SELECT AVG(p.precio_venta) FROM producto p WHERE p.proveedor=proveedor);
  END //
DELIMITER ;

SELECT ej8('Frutales Talavera S.A');
SELECT * FROM  producto p;


/*
  Ej9 - Realizar el mismo ejercicio anterior, pero haciendo que en caso 
        de que el fabricante pasado no tenga productos produzca una excepción
        personalizada que muestre el error fabricante no existe.   
*/
DELIMITER //
  CREATE OR REPLACE FUNCTION ej9(v_proveedor varchar(60))
    RETURNS float
    COMMENT 'Realizar el mismo ejercicio anterior, pero haciendo que en caso 
             de que el fabricante pasado no tenga productos produzca una excepción
             personalizada que muestre el error fabricante no existe.'
  BEGIN
    DECLARE contador int DEFAULT 0;
    DECLARE salida CONDITION FOR SQLSTATE '02000';

    SET contador = (SELECT COUNT(*) FROM producto WHERE proveedor = v_proveedor);
  
    IF (contador = 0) THEN
      SIGNAL salida SET MESSAGE_TEXT = 'El fabricante no existe';
    END IF;
  
    RETURN (SELECT
              AVG(precio_venta)
              FROM producto
              WHERE proveedor = v_proveedor);
  END //
DELIMITER ;

SELECT ej9('Frutales Talavera S.A');
  